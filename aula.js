import React from 'react';
import { View, Text, Image, ScrollView, TextInput } from 'react-native';

const aula = () => {
  return (
    <ScrollView>
      <Text>REACT-NATIVE</Text>
      <View>
        <Text>INI ADALAH SALAH SATU CONTOH SCROOLVIEW</Text>
        <Image
          source={{
            uri: 'https://reactnative.dev/docs/assets/p_cat2.png',
          }}
          style={{ width: 200, height: 200 }}
        />
      </View>
      <TextInput
        style={{
          height: 40,
          borderColor: 'gray',
          borderWidth: 1
        }}
        defaultValue="COBA KETIKKAN SESUATU"
      />
    </ScrollView>
  );
}

export default aula;